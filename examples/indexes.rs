use persy::{Persy, PersyError, PersyId, Value, ValueMode};
use std::path::Path;

///
/// Example of insert a record and create a side index that link to the specific record
///
///
fn main() -> Result<(), PersyError> {
    let create_segment;
    if !Path::new("index.exp").exists() {
        Persy::create("index.exp")?;
        create_segment = true;
    } else {
        create_segment = false;
    }

    let persy = Persy::open("index.exp", persy::Config::new())?;
    if create_segment {
        let mut tx = persy.begin()?;
        tx.create_segment("data")?;
        tx.create_index::<String, PersyId>("index", ValueMode::REPLACE)?;
        let prepared = tx.prepare_commit()?;
        prepared.commit()?;
    }
    let mut tx = persy.begin()?;
    let rec = "aaaa".as_bytes();
    let id = tx.insert_record("data", rec)?;

    tx.put::<String, PersyId>("index", "key".to_string(), id)?;
    let prepared = tx.prepare_commit()?;
    prepared.commit()?;

    let read_id = persy.get::<String, PersyId>("index", &"key".to_string())?;
    if let Some(is_there) = read_id {
        if let Value::SINGLE(id) = is_there {
            let value = persy.read_record("data", &id)?;
            assert_eq!(Some(rec.to_vec()), value);
        }
    }
    Ok(())
}
