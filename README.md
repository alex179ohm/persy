# Persy
[![build status](https://gitlab.com/tglman/persy/badges/master/build.svg)](https://gitlab.com/tglman/persy/commits/master)
[![coverage report](https://gitlab.com/tglman/persy/badges/master/coverage.svg)](https://gitlab.com/tglman/persy/commits/master)

Persy is a transactional storage engine written in rust.

[persy.rs](https://persy.rs)  
[crates.io](https://crates.io/crates/persy)  
[docs.rs](https://docs.rs/persy/)  
[Mastodon](https://mastodon.technology/@persy_rs)  

## COMPILING THE SOURCE

Checkout the source code:

```
git clone https://gitlab.com/tglman/persy.git
```

Compile and Test

```
cargo test
```

## INSTALL

Add it as dependency of your project:

```toml
[dependencies]
persy="0.10"
```

## USAGE EXAMPLE

Create a new persy file save some data in it and scan it.

```rust
use persy::{Persy,Config};
//...
Persy::create("./open.persy")?;
let persy = Persy::open("./open.persy",Config::new())?;
let mut tx = persy.begin()?;
tx.create_segment("seg")?;
let data = vec![1;20];
tx.insert_record("seg", &data)?;
let prepared = tx.prepare_commit()?;
prepared.commit()?;
for (_id, content) in persy.scan("seg")? {
    assert_eq!(content[0], 20);
    //....
}
```

## CONTRIBUTION

There are a few option for contribute to persy, you can start from reviewing and suggesting API changes, jump directly to hacking the code or just playing a bit with docs.
If you want a list of possibility you can start from the list of [new contributor friendly issues](https://gitlab.com/tglman/persy/issues?label_name%5B%5D=new+contributor+friendly)
If you would like to contact the developers send a mail to user `dev` on the `persy.rs` domain.
