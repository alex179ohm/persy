use crate::{
    id::SegmentId,
    persy::PersyImpl,
    segment::SegmentPageIterator,
    snapshot::SnapshotId,
    transaction::{Transaction, TransactionInsertIterator},
    PersyId,
};

pub struct SegmentRawIter {
    segment_id: SegmentId,
    iterator: SegmentPageIterator,
}

impl SegmentRawIter {
    pub fn new(segment_id: SegmentId, iterator: SegmentPageIterator) -> SegmentRawIter {
        SegmentRawIter { segment_id, iterator }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl) -> Option<(PersyId, Vec<u8>)> {
        while let Some(id) = self.iterator.next(persy_impl.address()) {
            if let Ok(Some(val)) = persy_impl.read_record(self.segment_id, &id) {
                return Some((PersyId(id), val));
            }
        }
        None
    }
}

pub struct SegmentSnapshotRawIter {
    segment_id: SegmentId,
    iterator: SegmentPageIterator,
    snapshot_id: SnapshotId,
}

impl SegmentSnapshotRawIter {
    pub fn new(
        segment_id: SegmentId,
        iterator: SegmentPageIterator,
        snapshot_id: SnapshotId,
    ) -> SegmentSnapshotRawIter {
        SegmentSnapshotRawIter {
            segment_id,
            iterator,
            snapshot_id,
        }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl) -> Option<(PersyId, Vec<u8>)> {
        while let Some(id) = self.iterator.next(persy_impl.address()) {
            if let Ok(Some(val)) = persy_impl.read_record_snapshot(self.segment_id, &id, self.snapshot_id) {
                return Some((PersyId(id), val));
            }
        }
        None
    }
}

pub struct TxSegmentRawIter {
    segment_id: SegmentId,
    tx_iterator: TransactionInsertIterator,
    iterator: SegmentPageIterator,
}

impl TxSegmentRawIter {
    pub fn new(tx: &Transaction, segment_id: SegmentId, iterator: SegmentPageIterator) -> TxSegmentRawIter {
        let iter = tx.scan_insert(segment_id.id).into_iter();
        TxSegmentRawIter {
            segment_id,
            tx_iterator: iter,
            iterator,
        }
    }
    pub fn next(&mut self, persy_impl: &PersyImpl, tx: &Transaction) -> Option<(PersyId, Vec<u8>, u16)> {
        while let Some(id) = self
            .iterator
            .next(persy_impl.address())
            .or_else(|| self.tx_iterator.next())
        {
            if let Ok(Some((val, version))) = persy_impl.read_record_tx_internal(tx, self.segment_id, &id) {
                return Some((PersyId(id), val, version));
            }
        }
        None
    }
}
