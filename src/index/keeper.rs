use crate::index::{
    config::{ByteVec, IndexType, Indexes, ValueMode},
    serialization::{deserialize, serialize},
    tree::{compare, IndexApply, KeyChanges, Node, NodeRef, PageIter, PageIterBack, Value, ValueChange as TreeValue},
};
use crate::{
    error::{PRes, PersyError},
    id::{index_id_to_segment_id_data, index_id_to_segment_id_meta, IndexId, SegmentId},
    persy::PersyImpl,
    snapshot::SnapshotId,
    transaction::Transaction,
    PersyId,
};
use std::{
    cmp::Ordering,
    collections::{btree_map::Entry as BTreeEntry, hash_map::Entry, BTreeMap, HashMap},
    iter::DoubleEndedIterator,
    ops::{Bound, RangeBounds},
    rc::Rc,
    sync::Arc,
    vec::IntoIter,
};

#[derive(Clone, Debug, PartialEq)]
pub enum ValueChange<V> {
    ADD(V),
    REMOVE(Option<V>),
}

#[derive(Clone)]
pub enum Change {
    ADD(usize),
    REMOVE(Option<usize>),
}

#[derive(Clone)]
pub struct Changes {
    changes: Vec<Change>,
}

impl Changes {
    fn new(change: Change) -> Changes {
        Changes { changes: vec![change] }
    }
    fn push(&mut self, change: Change) {
        self.changes.push(change);
    }
}

fn add_value<V: IndexType>(values: &mut ValueContainer, val: V) -> usize {
    let v = V::Wrapper::get_vec_mut(values);
    let l = v.len();
    v.push(val.into());
    l
}
fn resolve_values<V: IndexType>(values: &ValueContainer, changes: Changes) -> Vec<ValueChange<V>> {
    let v = V::Wrapper::get_vec(values);
    changes
        .changes
        .iter()
        .map(|c| match c {
            Change::ADD(p) => ValueChange::ADD(v[*p].clone().into()),
            Change::REMOVE(o) => ValueChange::REMOVE(o.map(|p| v[p].clone().into())),
        })
        .collect()
}
fn add_entry<K: IndexType>(entries: &mut EntriesContainer, k: K, change: Change) {
    let v = K::Wrapper::get_entries_mut(entries).expect("wrong match from the type and the value container");
    match v.entry(k.into()) {
        BTreeEntry::Occupied(ref mut o) => {
            o.get_mut().push(change);
        }
        BTreeEntry::Vacant(va) => {
            va.insert(Changes::new(change));
        }
    }
}

fn get_changes<K: IndexType>(entries: &EntriesContainer, x: &K) -> Option<Changes> {
    if let Some(v) = K::Wrapper::get_entries(entries) {
        let k = x.clone().into();
        v.get(&k).map(Clone::clone)
    } else {
        None
    }
}

fn resolve_range<T: IndexType, R>(entries: &EntriesContainer, range: R) -> IntoIter<T>
where
    R: RangeBounds<T>,
{
    let into_range: (Bound<T::Wrapper>, Bound<T::Wrapper>) =
        (map_bound(range.start_bound()), map_bound(range.end_bound()));
    let v = T::Wrapper::get_entries(entries).expect("wrong match from the type and the value container");
    BTreeMap::range(v, into_range)
        .map(|x| x.0.clone().into())
        .collect::<Vec<T>>()
        .into_iter()
}

macro_rules! impl_index_data_type {
    ($t:ty, $v:path, $v2:path) => {
        impl Extractor for <$t as IndexType>::Wrapper {
            fn get_vec_mut(vc: &mut ValueContainer) -> &mut Vec<Self> {
                if let $v(ref mut v) = vc {
                    v
                } else {
                    panic!("wrong match from type and value container")
                }
            }
            fn get_vec(vc: &ValueContainer) -> &Vec<Self> {
                if let $v(ref v) = vc {
                    v
                } else {
                    panic!("wrong match from type and value container")
                }
            }

            fn get_entries(vc: &EntriesContainer) -> Option<&BTreeMap<Self, Changes>> {
                if let $v2(ref v) = vc {
                    Some(v)
                } else {
                    None
                }
            }
            fn get_entries_mut(vc: &mut EntriesContainer) -> Option<&mut BTreeMap<Self, Changes>> {
                if let $v2(ref mut v) = vc {
                    Some(v)
                } else {
                    None
                }
            }
            fn new_entries() -> EntriesContainer {
                $v2(BTreeMap::new())
            }

            fn new_values() -> ValueContainer {
                $v(Vec::new())
            }
        }
    };
}

macro_rules! container_enums {
    ($($variant:ident<$t:ty>),+,) => {
        #[derive(Clone)]
        pub enum EntriesContainer {
            $(
            $variant(BTreeMap<<$t as IndexType>::Wrapper, Changes>),
            )+
        }

        #[derive(Clone)]
        pub enum ValueContainer {
            $(
            $variant(Vec<<$t as IndexType>::Wrapper>),
            )+
        }

        fn eapplier(
            keys: &EntriesContainer,
            values: &ValueContainer,
            index_id: &IndexId,
            persy: &PersyImpl,
            tx: &mut Transaction,
        ) -> PRes<()> {
            match keys {
                $(
                EntriesContainer::$variant(k) => valapplier::<$t>(values, k, index_id, persy, tx),
                )+
            }
        }

        fn valapplier<K>(
            values: &ValueContainer,
            k: &BTreeMap<K::Wrapper, Changes>,
            index_id: &IndexId,
            persy: &PersyImpl,
            tx: &mut Transaction,
        ) -> PRes<()>
        where
            K: IndexType,
        {
            match values {
                $(
                ValueContainer::$variant(v) => apply_to_index::<K, $t>(persy, tx, index_id, k, v),
                )+
            }
        }

        $(
            impl_index_data_type!($t, ValueContainer::$variant, EntriesContainer::$variant);
        )+
    }
}

container_enums!(
    U8<u8>,
    U16<u16>,
    U32<u32>,
    U64<u64>,
    U128<u128>,
    I8<i8>,
    I16<i16>,
    I32<i32>,
    I64<i64>,
    I128<i128>,
    F32W<f32>,
    F64W<f64>,
    STRING<String>,
    PERSYID<PersyId>,
    BYTEVEC<ByteVec>,
);

pub trait Extractor: Sized {
    fn get_vec_mut(vc: &mut ValueContainer) -> &mut Vec<Self>;
    fn get_vec(vc: &ValueContainer) -> &Vec<Self>;
    fn get_entries(vc: &EntriesContainer) -> Option<&BTreeMap<Self, Changes>>;
    fn get_entries_mut(vc: &mut EntriesContainer) -> Option<&mut BTreeMap<Self, Changes>>;
    fn new_entries() -> EntriesContainer;
    fn new_values() -> ValueContainer;
}

fn apply_to_index<K, V>(
    persy: &PersyImpl,
    tx: &mut Transaction,
    index_id: &IndexId,
    keys: &BTreeMap<K::Wrapper, Changes>,
    values: &[V::Wrapper],
) -> PRes<()>
where
    K: IndexType,
    V: IndexType,
{
    let changes: Vec<_> = keys
        .iter()
        .map(|(k, c)| {
            let vals: Vec<_> = c
                .changes
                .iter()
                .map(|ch| match *ch {
                    Change::ADD(pos) => TreeValue::ADD(values[pos].clone().into()),
                    Change::REMOVE(pos) => TreeValue::REMOVE(pos.map(|p| values[p].clone().into())),
                })
                .collect();
            KeyChanges::new(k.clone().into(), vals)
        })
        .collect();
    let mut index = Indexes::get_index_keeper_tx::<K, V>(persy, tx, index_id)?;
    index.apply(&changes)?;
    index.update_changed()?;
    Ok(())
}

fn map_bound<T: Into<T1> + Clone, T1>(b: Bound<&T>) -> Bound<T1> {
    match b {
        Bound::Excluded(x) => Bound::Excluded((*x).clone().into()),
        Bound::Included(x) => Bound::Included((*x).clone().into()),
        Bound::Unbounded => Bound::Unbounded,
    }
}

pub struct IndexTransactionKeeper {
    indexex_changes: HashMap<IndexId, (EntriesContainer, ValueContainer)>,
}

impl IndexTransactionKeeper {
    pub fn new() -> IndexTransactionKeeper {
        IndexTransactionKeeper {
            indexex_changes: HashMap::new(),
        }
    }

    pub fn put<K, V>(&mut self, index: IndexId, k: K, v: V)
    where
        K: IndexType,
        V: IndexType,
    {
        match self.indexex_changes.entry(index) {
            Entry::Occupied(ref mut o) => {
                let (entries, values) = o.get_mut();
                let pos = add_value(values, v);
                add_entry(entries, k, Change::ADD(pos));
            }
            Entry::Vacant(va) => {
                let mut values = V::Wrapper::new_values();
                let mut keys = K::Wrapper::new_entries();
                let pos = add_value(&mut values, v);
                add_entry(&mut keys, k, Change::ADD(pos));
                va.insert((keys, values));
            }
        }
    }

    pub fn remove<K, V>(&mut self, index: IndexId, k: K, v: Option<V>)
    where
        K: IndexType,
        V: IndexType,
    {
        match self.indexex_changes.entry(index) {
            Entry::Occupied(ref mut o) => {
                let pos = if let Some(val) = v {
                    Some(add_value(&mut o.get_mut().1, val))
                } else {
                    None
                };
                add_entry(&mut o.get_mut().0, k, Change::REMOVE(pos));
            }
            Entry::Vacant(va) => {
                let mut values = V::Wrapper::new_values();
                let mut keys = K::Wrapper::new_entries();
                let pos = if let Some(val) = v {
                    Some(add_value(&mut values, val))
                } else {
                    None
                };
                add_entry(&mut keys, k, Change::REMOVE(pos));
                va.insert((keys, values));
            }
        }
    }

    pub fn get_changes<K, V>(&self, index: IndexId, k: &K) -> Option<Vec<ValueChange<V>>>
    where
        K: IndexType,
        V: IndexType,
    {
        self.indexex_changes
            .get(&index)
            .map(|ref o| get_changes(&o.0, k).map(|c| resolve_values(&o.1, c)))
            .and_then(std::convert::identity)
    }

    pub fn apply_changes<K, V>(
        &self,
        index_id: IndexId,
        vm: ValueMode,
        k: &K,
        pers: Option<Value<V>>,
    ) -> PRes<Option<Value<V>>>
    where
        K: IndexType,
        V: IndexType,
    {
        let mut result = pers;
        if let Some(key_changes) = self.get_changes::<K, V>(index_id, k) {
            for change in key_changes {
                result = match change {
                    ValueChange::ADD(add_value) => Some(if let Some(s_result) = result {
                        match s_result {
                            Value::SINGLE(v) => match vm {
                                ValueMode::REPLACE => Value::SINGLE(add_value),
                                ValueMode::EXCLUSIVE => {
                                    if compare(&v, &add_value) == Ordering::Equal {
                                        Value::SINGLE(v)
                                    } else {
                                        // TODO: recover index name
                                        return Err(PersyError::IndexDuplicateKey("".to_string(), format!("{}", k)));
                                    }
                                }
                                ValueMode::CLUSTER => {
                                    if compare(&v, &add_value) == Ordering::Equal {
                                        Value::SINGLE(v)
                                    } else {
                                        Value::CLUSTER(vec![v, add_value])
                                    }
                                }
                            },
                            Value::CLUSTER(mut values) => {
                                if let Ok(pos) = values.binary_search_by(|x| compare(x, &add_value)) {
                                    values.insert(pos, add_value);
                                }
                                Value::CLUSTER(values)
                            }
                        }
                    } else {
                        Value::SINGLE(add_value)
                    }),
                    ValueChange::REMOVE(rv) => rv.and_then(|remove_value| {
                        result.and_then(|s_result| match s_result {
                            Value::SINGLE(v) => {
                                if compare(&v, &remove_value) == Ordering::Equal {
                                    None
                                } else {
                                    Some(Value::SINGLE(v))
                                }
                            }
                            Value::CLUSTER(mut values) => {
                                if let Ok(pos) = values.binary_search_by(|x| compare(x, &remove_value)) {
                                    values.remove(pos);
                                }
                                Some(if values.len() == 1 {
                                    Value::SINGLE(values.pop().unwrap())
                                } else {
                                    Value::CLUSTER(values)
                                })
                            }
                        })
                    }),
                };
            }
        }
        Ok(result)
    }

    pub fn apply(&self, persy: &PersyImpl, tx: &mut Transaction) -> PRes<()> {
        for (index, values) in &self.indexex_changes {
            eapplier(&values.0, &values.1, index, persy, tx)?;
        }
        Ok(())
    }

    pub fn range<K, V, R>(&self, index: IndexId, range: R) -> Option<IntoIter<K>>
    where
        K: IndexType,
        V: IndexType,
        R: RangeBounds<K>,
    {
        self.indexex_changes.get(&index).map(|x| resolve_range(&x.0, range))
    }

    pub fn changed_indexes(&self) -> Vec<IndexId> {
        self.indexex_changes.keys().cloned().collect()
    }
    pub fn remove_changes(&mut self, index_id: &IndexId) {
        self.indexex_changes.remove(index_id);
    }
}

pub trait IndexKeeper<K, V> {
    fn load(&self, node: &NodeRef) -> PRes<Node<K, V>>;
    fn get_root(&self) -> PRes<Option<NodeRef>>;
    fn value_mode(&self) -> ValueMode;
    fn index_name(&self) -> &String;
}

pub trait IndexModify<K, V>: IndexKeeper<K, V> {
    fn load_modify(&self, node: &NodeRef) -> PRes<Option<(Rc<Node<K, V>>, u16)>>;
    fn lock(&mut self, node: &NodeRef, version: u16) -> PRes<bool>;
    fn owned(&mut self, node_ref: &NodeRef, node: Rc<Node<K, V>>) -> Node<K, V>;
    fn unlock(&mut self, node: &NodeRef) -> PRes<bool>;
    fn unlock_config(&mut self) -> PRes<bool>;
    fn get_root_refresh(&mut self) -> PRes<Option<NodeRef>>;
    fn lock_config(&mut self) -> PRes<bool>;
    fn insert(&mut self, node: Node<K, V>) -> PRes<NodeRef>;
    fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>, version: u16) -> PRes<()>;
    fn delete(&mut self, node: &NodeRef, version: u16) -> PRes<()>;
    fn set_root(&mut self, root: Option<NodeRef>) -> PRes<()>;
    fn bottom_limit(&self) -> usize;
    fn top_limit(&self) -> usize;
}

pub struct IndexSegmentKeeper<'a> {
    name: String,
    segment: SegmentId,
    root: Option<NodeRef>,
    store: &'a PersyImpl,
    snapshot: SnapshotId,
    value_mode: ValueMode,
}

impl<'a> IndexSegmentKeeper<'a> {
    pub fn new(
        name: &str,
        index_id: &IndexId,
        root: Option<NodeRef>,
        store: &'a PersyImpl,
        snapshot: SnapshotId,
        value_mode: ValueMode,
    ) -> IndexSegmentKeeper<'a> {
        IndexSegmentKeeper {
            name: name.to_string(),
            segment: index_id_to_segment_id_data(index_id),
            root,
            store,
            snapshot,
            value_mode,
        }
    }
}

impl<'a, K: IndexType, V: IndexType> IndexKeeper<K, V> for IndexSegmentKeeper<'a> {
    fn load(&self, node: &NodeRef) -> PRes<Node<K, V>> {
        let rec = self
            .store
            .read_record_snapshot_fn(self.segment, &node, self.snapshot, |x| deserialize(x))?
            .unwrap()?;
        Ok(rec)
    }
    fn get_root(&self) -> PRes<Option<NodeRef>> {
        Ok(self.root.clone())
    }
    fn value_mode(&self) -> ValueMode {
        self.value_mode.clone()
    }

    fn index_name(&self) -> &String {
        &self.name
    }
}

struct LockData {
    version: u16,
    counter: u32,
}

pub struct IndexSegmentKeeperTx<'a, K: IndexType, V: IndexType> {
    name: String,
    index_id: IndexId,
    root: Option<NodeRef>,
    config_version: u16,
    store: &'a PersyImpl,
    tx: &'a mut Transaction,
    value_mode: ValueMode,
    changed: Option<HashMap<NodeRef, (Rc<Node<K, V>>, u16)>>,
    bottom_limit: usize,
    top_limit: usize,
    locked: HashMap<NodeRef, LockData>,
    updated_root: bool,
}

impl<'a, K: IndexType, V: IndexType> IndexSegmentKeeperTx<'a, K, V> {
    pub fn new(
        name: &str,
        index_id: &IndexId,
        root: Option<NodeRef>,
        config_version: u16,
        store: &'a PersyImpl,
        tx: &'a mut Transaction,
        value_mode: ValueMode,
        bottom_limit: usize,
        top_limit: usize,
    ) -> IndexSegmentKeeperTx<'a, K, V> {
        IndexSegmentKeeperTx {
            name: name.to_string(),
            index_id: index_id.clone(),
            root,
            config_version,
            store,
            tx,
            value_mode,
            changed: None,
            bottom_limit,
            top_limit,
            locked: HashMap::new(),
            updated_root: false,
        }
    }
    pub fn update_changed(&mut self) -> PRes<()> {
        let segment = index_id_to_segment_id_data(&self.index_id);
        if let Some(m) = &self.changed {
            for (node_ref, node) in m {
                self.store
                    .update_record(self.tx, segment, &node_ref, &serialize(&node.0)?)?;
            }
        }
        if self.updated_root {
            Indexes::update_index_root(self.store, self.tx, &self.index_id, self.root.clone())?;
        }
        Ok(())
    }
}

impl<'a, K: IndexType, V: IndexType> IndexModify<K, V> for IndexSegmentKeeperTx<'a, K, V> {
    fn load_modify(&self, node: &NodeRef) -> PRes<Option<(Rc<Node<K, V>>, u16)>> {
        if let Some(m) = &self.changed {
            if let Some(n) = m.get(node) {
                return Ok(Some(n.clone()));
            }
        }
        let segment = index_id_to_segment_id_data(&self.index_id);
        if let Some((rec, version)) = self
            .store
            .read_record_tx_internal_fn(self.tx, segment, &node, |x| deserialize(x))?
        {
            Ok(Some((Rc::new(rec?), version)))
        } else {
            Ok(None)
        }
    }
    fn lock(&mut self, node: &NodeRef, version: u16) -> PRes<bool> {
        if let Some(lock_data) = self.locked.get_mut(node) {
            if version == lock_data.version {
                lock_data.counter += 1;
                Ok(true)
            } else if version < lock_data.version {
                Ok(false)
            } else {
                panic!("wrong matched versions {} {}", version, lock_data.version);
            }
        } else {
            let segment = index_id_to_segment_id_data(&self.index_id);
            if self.store.lock_record(self.tx, &segment, node, version)? {
                self.locked.insert(node.clone(), LockData { version, counter: 1 });
                Ok(true)
            } else {
                Ok(false)
            }
        }
    }

    fn owned(&mut self, node_ref: &NodeRef, mut node: Rc<Node<K, V>>) -> Node<K, V> {
        debug_assert!(self.locked.contains_key(node_ref));
        if let Some(changed) = &mut self.changed {
            changed.remove(node_ref);
        }
        Rc::make_mut(&mut node);
        Rc::try_unwrap(node).ok().unwrap()
    }

    fn unlock(&mut self, node: &NodeRef) -> PRes<bool> {
        if let Entry::Occupied(mut x) = self.locked.entry(node.clone()) {
            x.get_mut().counter -= 1;
            if x.get().counter == 0 {
                x.remove();
                let segment = index_id_to_segment_id_data(&self.index_id);
                self.store.unlock_record(self.tx, &segment, node)?;
                Ok(true)
            } else {
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }

    fn get_root_refresh(&mut self) -> PRes<Option<NodeRef>> {
        if !self.updated_root {
            let (config, version) = Indexes::get_index_tx(self.store, self.tx, &self.index_id)?;
            self.root = config.get_root();
            self.config_version = version;
        }
        Ok(self.root.clone())
    }
    fn unlock_config(&mut self) -> PRes<bool> {
        let config_id = Indexes::get_config_id(self.store, self.tx, &self.index_id)?.0;
        if let Entry::Occupied(mut x) = self.locked.entry(config_id.clone()) {
            x.get_mut().counter -= 1;
            if x.get().counter == 0 {
                x.remove();
                let segment = index_id_to_segment_id_meta(&self.index_id);
                self.store.unlock_record(self.tx, &segment, &config_id)?;
                Ok(true)
            } else {
                Ok(false)
            }
        } else {
            Ok(false)
        }
    }
    fn lock_config(&mut self) -> PRes<bool> {
        let config_id = Indexes::get_config_id(self.store, self.tx, &self.index_id)?.0;

        let segment = index_id_to_segment_id_meta(&self.index_id);
        if let Some(lock_data) = self.locked.get_mut(&config_id) {
            if self.config_version == lock_data.version {
                lock_data.counter += 1;
                Ok(true)
            } else {
                panic!("this should never happen");
            }
        } else if self
            .store
            .lock_record(self.tx, &segment, &config_id, self.config_version)?
        {
            self.locked.insert(
                config_id.clone(),
                LockData {
                    version: self.config_version,
                    counter: 1,
                },
            );
            Ok(true)
        } else {
            let (config, version) = Indexes::get_index_tx(self.store, self.tx, &self.index_id)?;
            self.root = config.get_root();
            self.config_version = version;
            Ok(false)
        }
    }

    fn insert(&mut self, node: Node<K, V>) -> PRes<NodeRef> {
        let segment = index_id_to_segment_id_data(&self.index_id);
        let node_ref = self.store.insert_record(self.tx, &segment, &serialize(&node)?)?;
        self.changed
            .get_or_insert_with(HashMap::new)
            .insert(node_ref.clone(), (Rc::new(node), 0));
        self.locked
            .insert(node_ref.clone(), LockData { version: 0, counter: 1 });
        Ok(node_ref)
    }

    fn update(&mut self, node_ref: &NodeRef, node: Node<K, V>, version: u16) -> PRes<()> {
        debug_assert!(self.locked.contains_key(node_ref));
        self.changed
            .get_or_insert_with(HashMap::new)
            .insert(node_ref.clone(), (Rc::new(node), version));
        Ok(())
    }

    fn delete(&mut self, node: &NodeRef, _version: u16) -> PRes<()> {
        debug_assert!(self.locked.contains_key(node));
        if let Some(m) = &mut self.changed {
            m.remove(node);
        }
        let segment = index_id_to_segment_id_data(&self.index_id);
        self.store.delete_record(self.tx, segment, &node)?;
        Ok(())
    }
    fn set_root(&mut self, root: Option<NodeRef>) -> PRes<()> {
        self.root = root;
        self.updated_root = true;
        Ok(())
    }

    fn bottom_limit(&self) -> usize {
        self.bottom_limit
    }
    fn top_limit(&self) -> usize {
        self.top_limit
    }
}

impl<'a, K: IndexType, V: IndexType> IndexKeeper<K, V> for IndexSegmentKeeperTx<'a, K, V> {
    fn load(&self, node: &NodeRef) -> PRes<Node<K, V>> {
        if let Some(m) = &self.changed {
            if let Some(n) = m.get(node) {
                return Ok(n.0.as_ref().clone());
            }
        }
        let segment = index_id_to_segment_id_data(&self.index_id);
        let (rec, _) = self
            .store
            .read_record_tx_internal_fn(self.tx, segment, &node, |x| deserialize(x))?
            .unwrap();
        Ok(rec?)
    }
    fn get_root(&self) -> PRes<Option<NodeRef>> {
        Ok(self.root.clone())
    }
    fn value_mode(&self) -> ValueMode {
        self.value_mode.clone()
    }

    fn index_name(&self) -> &String {
        &self.name
    }
}

/// Index Iterator implementation for iterating on a range of keys considering in transaction
/// changes
pub struct TxIndexRawIter<K: IndexType, V: IndexType> {
    index_name: IndexId,
    in_tx: Option<IntoIter<K>>,
    persistent: Option<IndexRawIter<K, V>>,
    in_tx_front: Option<Option<K>>,
    persistent_front: Option<Option<(K, Value<V>)>>,
    in_tx_back: Option<Option<K>>,
    persistent_back: Option<Option<(K, Value<V>)>>,
    value_mode: ValueMode,
}

impl<K, V> TxIndexRawIter<K, V>
where
    K: IndexType,
    V: IndexType,
{
    pub fn new(
        index_name: IndexId,
        in_tx: Option<IntoIter<K>>,
        persistent: Option<IndexRawIter<K, V>>,
        value_mode: ValueMode,
    ) -> TxIndexRawIter<K, V> {
        TxIndexRawIter {
            index_name,
            in_tx,
            persistent,
            in_tx_front: None,
            persistent_front: None,
            in_tx_back: None,
            persistent_back: None,
            value_mode,
        }
    }

    fn apply_changes(
        tx: &mut Transaction,
        vm: ValueMode,
        index: IndexId,
        k: K,
        pers: Option<Value<V>>,
    ) -> Option<(K, Value<V>)> {
        tx.apply_changes(vm, index, &k, pers).unwrap_or(None).map(|v| (k, v))
    }

    pub fn next(&mut self, persy_impl: &Arc<PersyImpl>, tx: &mut Transaction) -> Option<(K, Value<V>)> {
        loop {
            let vm = self.value_mode.clone();
            let index = self.index_name.clone();
            let apply_changes = |k, o| Self::apply_changes(tx, vm, index, k, o);
            match (&mut self.in_tx, &mut self.persistent) {
                (Some(it), Some(pers)) => {
                    match (
                        self.in_tx_front.get_or_insert_with(|| it.next()).clone(),
                        self.persistent_front
                            .get_or_insert_with(|| pers.next(persy_impl))
                            .clone(),
                    ) {
                        (Some(tx_k), Some((pers_k, vals))) => match tx_k.cmp(&pers_k) {
                            Ordering::Less => {
                                self.in_tx_front = None;
                                let res = apply_changes(tx_k, None);
                                if res.is_some() {
                                    break res;
                                }
                            }
                            Ordering::Equal => {
                                self.in_tx_front = None;
                                self.persistent_front = None;
                                let res = apply_changes(tx_k, Some(vals));

                                if res.is_some() {
                                    break res;
                                }
                            }
                            Ordering::Greater => {
                                self.persistent_front = None;
                                break Some((pers_k, vals));
                            }
                        },
                        (Some(tx_k), None) => {
                            self.in_tx_front = None;
                            let res = apply_changes(tx_k, None);
                            if res.is_some() {
                                break res;
                            }
                        }
                        (None, Some((pers_k, vals))) => {
                            self.persistent_front = None;
                            break Some((pers_k, vals));
                        }
                        (None, None) => break None,
                    }
                }
                (Some(it), None) => {
                    let res = apply_changes(it.next().unwrap(), None);
                    if res.is_some() {
                        break res;
                    }
                }
                (None, Some(pers)) => break pers.next(persy_impl),
                (None, None) => break None,
            }
        }
    }

    pub fn next_back(&mut self, persy_impl: &Arc<PersyImpl>, tx: &mut Transaction) -> Option<(K, Value<V>)> {
        loop {
            let vm = self.value_mode.clone();
            let index = self.index_name.clone();
            let apply_changes = |k, o| Self::apply_changes(tx, vm, index, k, o);
            match (&mut self.in_tx, &mut self.persistent) {
                (Some(it), Some(pers)) => {
                    match (
                        self.in_tx_back.get_or_insert_with(|| it.next_back()).clone(),
                        self.persistent_back
                            .get_or_insert_with(|| pers.next_back(persy_impl))
                            .clone(),
                    ) {
                        (Some(tx_k), Some((pers_k, vals))) => match tx_k.cmp(&pers_k) {
                            Ordering::Less => {
                                self.persistent_back = None;
                                break Some((pers_k, vals));
                            }
                            Ordering::Equal => {
                                self.in_tx_back = None;
                                self.persistent_back = None;
                                let res = apply_changes(tx_k, Some(vals));

                                if res.is_some() {
                                    break res;
                                }
                            }
                            Ordering::Greater => {
                                self.in_tx_back = None;
                                let res = apply_changes(tx_k, None);

                                if res.is_some() {
                                    break res;
                                }
                            }
                        },
                        (Some(tx_k), None) => {
                            self.in_tx_back = None;
                            let res = apply_changes(tx_k, None);
                            if res.is_some() {
                                break res;
                            }
                        }
                        (None, Some((pers_k, vals))) => {
                            self.persistent_back = None;
                            break Some((pers_k, vals));
                        }
                        (None, None) => break None,
                    }
                }
                (Some(it), None) => {
                    let res = apply_changes(it.next_back().unwrap(), None);
                    if res.is_some() {
                        break res;
                    }
                }
                (None, Some(pers)) => break pers.next_back(persy_impl),
                (None, None) => break None,
            }
        }
    }
}

pub struct IndexRawIter<K: IndexType, V: IndexType> {
    index_id: IndexId,
    read_snapshot: SnapshotId,
    iter: PageIter<K, V>,
    back: PageIterBack<K, V>,
    release_snapshot: bool,
}

impl<K, V> IndexRawIter<K, V>
where
    K: IndexType,
    V: IndexType,
{
    pub fn new(
        index_id: IndexId,
        read_snapshot: SnapshotId,
        iter: PageIter<K, V>,
        back: PageIterBack<K, V>,
        release_snapshot: bool,
    ) -> IndexRawIter<K, V> {
        IndexRawIter {
            index_id,
            read_snapshot,
            iter,
            back,
            release_snapshot,
        }
    }

    pub fn next(&mut self, persy_impl: &Arc<PersyImpl>) -> Option<(K, Value<V>)> {
        let back_keep = self.back.iter.peek();
        if let (Some(s), Some(e)) = (self.iter.iter.peek(), back_keep) {
            if s.key.cmp(&e.key) == Ordering::Greater {
                return None;
            }
        }
        if let Some(n) = self.iter.iter.next() {
            if self.iter.iter.peek().is_none() {
                if let Ok(iter) = persy_impl.index_next(&self.index_id, self.read_snapshot, Bound::Excluded(&n.key)) {
                    self.iter = iter;
                }
            }
            Some((n.key, n.value))
        } else {
            None
        }
    }

    pub fn next_back(&mut self, persy_impl: &Arc<PersyImpl>) -> Option<(K, Value<V>)> {
        let front_keep = self.iter.iter.peek();
        if let (Some(s), Some(e)) = (self.back.iter.peek(), front_keep) {
            if s.key.cmp(&e.key) == Ordering::Less {
                return None;
            }
        }
        if let Some(n) = self.back.iter.next() {
            if self.back.iter.peek().is_none() {
                if let Ok(back) = persy_impl.index_back(&self.index_id, self.read_snapshot, Bound::Excluded(&n.key)) {
                    self.back = back;
                }
            }
            Some((n.key, n.value))
        } else {
            None
        }
    }
    pub fn release(&self, persy_impl: &Arc<PersyImpl>) -> PRes<()> {
        if self.release_snapshot {
            persy_impl.release_snapshot(self.read_snapshot)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{ByteVec, IndexTransactionKeeper, IndexType, ValueChange};
    use crate::id::{IndexId, PersyId, RecRef};
    use std::fmt::Debug;

    fn keeper_test_for_type<K: IndexType + PartialEq, V: IndexType + Debug + PartialEq>(k: K, dk: K, v: V) {
        let name = IndexId::new(30, 40);
        let mut keeper = IndexTransactionKeeper::new();
        keeper.put(name.clone(), k.clone(), v.clone());
        let ret = keeper.get_changes(name.clone(), &k);
        assert_eq!(ret, Some(vec![ValueChange::ADD(v.clone())]));
        keeper.remove(name.clone(), dk.clone(), Some(v.clone()));
        let ret = keeper.get_changes(name, &dk);
        assert_eq!(ret, Some(vec![ValueChange::REMOVE(Some(v))]));
    }

    #[test]
    fn simple_tx_keeper_test() {
        keeper_test_for_type::<u8, u8>(10, 15, 10);
        keeper_test_for_type::<u16, u16>(10, 15, 10);
        keeper_test_for_type::<u32, u32>(10, 15, 10);
        keeper_test_for_type::<u64, u64>(10, 15, 10);
        keeper_test_for_type::<u128, u128>(10, 15, 10);
        keeper_test_for_type::<i8, i8>(10, 15, 10);
        keeper_test_for_type::<i16, i16>(10, 15, 10);
        keeper_test_for_type::<i32, i32>(10, 15, 10);
        keeper_test_for_type::<i64, i64>(10, 15, 10);
        keeper_test_for_type::<i128, i128>(10, 15, 10);
        keeper_test_for_type::<f32, f32>(20.0, 10.0, 20.0);
        keeper_test_for_type::<f64, f64>(20.0, 10.0, 20.0);
        keeper_test_for_type::<String, String>("a".to_string(), "b".to_string(), "a".to_string());
        keeper_test_for_type::<ByteVec, ByteVec>(vec![0, 1].into(), vec![0, 2].into(), vec![0, 1].into());
        let id = PersyId(RecRef::new(10, 20));
        let id1 = PersyId(RecRef::new(20, 20));
        let id2 = PersyId(RecRef::new(30, 20));
        keeper_test_for_type::<PersyId, PersyId>(id, id1, id2);
    }
}
