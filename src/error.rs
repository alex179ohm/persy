pub use std::fs::OpenOptions;
use std::{borrow::Cow, error, fmt, io, str, sync};

use crate::id::PersyId;
use unsigned_varint::decode::Error as VarintError;

/// Enum of possible errors from Persy
#[derive(Debug)]
pub enum PersyError {
    Io(io::Error),
    DecodingUtf8(str::Utf8Error),
    DecodingVarint(VarintError),
    DecodingDataEncoding(data_encoding::DecodeError),
    Custom(Box<dyn error::Error + Send + Sync + 'static>),
    VersionNotLastest,
    RecordNotFound(PersyId),
    SegmentNotFound,
    SegmentAlreadyExists,
    CannotDropSegmentCreatedInTx,
    Lock,
    IndexMinElementsShouldBeAtLeastDoubleOfMax,
    IndexNotFound,
    IndexTypeMismatch(Cow<'static, str>),
    IndexDuplicateKey(String, String),
    TransactionTimeout,

    #[doc(hidden)]
    __NonExhausive,
}

pub type PRes<T> = Result<T, PersyError>;

impl<T> From<sync::PoisonError<T>> for PersyError {
    fn from(_: sync::PoisonError<T>) -> PersyError {
        PersyError::Lock
    }
}

impl From<io::Error> for PersyError {
    fn from(err: io::Error) -> PersyError {
        PersyError::Io(err)
    }
}

impl From<str::Utf8Error> for PersyError {
    fn from(err: str::Utf8Error) -> PersyError {
        PersyError::DecodingUtf8(err)
    }
}

impl From<data_encoding::DecodeError> for PersyError {
    fn from(err: data_encoding::DecodeError) -> PersyError {
        PersyError::DecodingDataEncoding(err)
    }
}

impl From<VarintError> for PersyError {
    fn from(err: VarintError) -> PersyError {
        PersyError::DecodingVarint(err)
    }
}

impl From<Box<dyn error::Error + Send + Sync + 'static>> for PersyError {
    fn from(err: Box<dyn error::Error + Send + Sync + 'static>) -> PersyError {
        PersyError::Custom(err)
    }
}

impl error::Error for PersyError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        use PersyError::*;
        match self {
            Io(ref e) => Some(e),
            DecodingUtf8(ref e) => Some(e),
            DecodingVarint(ref e) => Some(e),
            DecodingDataEncoding(ref e) => Some(e),
            Custom(ref e) => Some(e.as_ref()),
            _ => None,
        }
    }
}

impl fmt::Display for PersyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use PersyError::*;
        match self {
            Io(m) => write!(f, "IO Error: {}", m),
            DecodingUtf8(e) => write!(f, "String decoding error: {}", e),
            DecodingDataEncoding(e) => write!(f, "Data Encoding Decoding error: {}", e),
            DecodingVarint(e) => write!(f, "Varint decoding error: {}", e),
            Custom(e) => write!(f, "{}", e),
            VersionNotLastest => write!(f, "The record version is not latest"),
            RecordNotFound(r) => write!(f, "Record not found: {}", r),

            SegmentNotFound => write!(f, "Segment not found"),

            SegmentAlreadyExists => write!(f, "Segment already exist"),

            CannotDropSegmentCreatedInTx => {
                write!(f, "Create and drop of a segment in the same transaction is not allowed")
            }

            Lock => write!(f, "Failure acquiring lock for poisoning"),

            IndexMinElementsShouldBeAtLeastDoubleOfMax => write!(
                f,
                "Index min page elements should be maximum half of the maximum elements"
            ),

            IndexNotFound => write!(f, "Index not found"),
            IndexTypeMismatch(m) => write!(f, "Index method type mismatch persistent types: {}", m),

            IndexDuplicateKey(i, k) => write!(f, "Found duplicate key:{} for index: {}", k, i),
            TransactionTimeout => write!(f, "Timeout acquiring the data locks for the transaction"),

            __NonExhausive => write!(f, "__non-Exhausive"),
        }
    }
}
