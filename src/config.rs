use std::time::Duration;

/// Concurrent Modification Strategy for resolution of conflict on commit.
///
#[derive(PartialEq, Clone, Debug)]
pub enum TxStrategy {
    ///
    /// Last modification received override all the previous modifications
    ///
    LastWin,

    ///
    /// prepare_commit will fail if the persistent version is more recent of the version when
    /// the update_record/delete_record is executed
    ///
    VersionOnWrite,

    ///
    /// prepare_commit will fail if the persistent version is more recent of the version of the
    /// last read_record_tx, if no read_record_tx was called will fallow the same behaviour of
    /// VersionOnWrite
    ///
    VersionOnRead,
}

impl TxStrategy {
    pub fn value(&self) -> u8 {
        match *self {
            TxStrategy::LastWin => 1,
            TxStrategy::VersionOnWrite => 2,
            TxStrategy::VersionOnRead => 3,
        }
    }
    pub fn from_value(val: u8) -> TxStrategy {
        match val {
            1 => TxStrategy::LastWin,
            2 => TxStrategy::VersionOnWrite,
            3 => TxStrategy::VersionOnRead,
            _ => panic!("something went wrong in tx strategy serialization: {}", val),
        }
    }
}

/// Persy configuration structure.
///
/// Lock are taken in order, should never go in deadlock so the default timeout is huge.
/// Current default values:
///
/// cache_size = 32M  
/// transaction_lock_timeout = 1 Day
/// concurrent_modification_strategy = LastWin  
///
#[derive(Debug, Clone)]
pub struct Config {
    cache_size: u64,
    transaction_lock_timeout: Duration,
    tx_strategy: TxStrategy,
}

impl Config {
    pub fn new() -> Config {
        Config {
            cache_size: 32 * 1024 * 1024,
            transaction_lock_timeout: Duration::new(24 * 60 * 60, 0),
            tx_strategy: TxStrategy::LastWin,
        }
    }

    pub fn cache_size(&self) -> u64 {
        self.cache_size
    }

    pub fn transaction_lock_timeout(&self) -> &Duration {
        &self.transaction_lock_timeout
    }

    pub fn change_cache_size(&mut self, cache_size: u64) {
        self.cache_size = cache_size;
    }

    pub fn change_transaction_lock_timeout(&mut self, transaction_lock_timeout: Duration) {
        self.transaction_lock_timeout = transaction_lock_timeout;
    }

    pub fn tx_strategy(&self) -> &TxStrategy {
        &self.tx_strategy
    }

    pub fn change_tx_strategy(&mut self, strategy: TxStrategy) {
        self.tx_strategy = strategy;
    }
}

impl Default for Config {
    fn default() -> Self {
        Self::new()
    }
}
