use crate::error::PRes;
use byteorder::{BigEndian, ByteOrder, ReadBytesExt, WriteBytesExt};
use std::{
    cmp,
    fs::File,
    io::{self, Cursor, Read, Seek, SeekFrom, Write},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, Mutex,
    },
};
pub const PAGE_METADATA_SIZE: u32 = 2;

pub trait UpdateList {
    fn update(&mut self, exp: u8, page: u64) -> PRes<u64>;
}

pub trait Device: Sync + Send {
    fn load_page(&self, page: u64) -> PRes<ReadPage>;

    /// Load a page avoiding to skip base page metadata, used for root page or metadata
    /// manipulation.
    ///
    fn load_page_raw(&self, page: u64, size_exp: u8) -> PRes<Page>;

    fn flush_page(&self, page: &Page) -> PRes<()>;

    /// Create a page without setting metadata, used by root page
    fn create_page_raw(&self, exp: u8) -> PRes<u64>;

    fn create_page(&self, exp: u8) -> PRes<Page>;

    fn mark_allocated(&self, page: u64) -> PRes<u64>;

    fn sync(&self) -> PRes<()>;

    fn trim_or_free_page(&self, page: u64, update_list: &mut dyn UpdateList) -> PRes<()>;
}

trait SizeTool {
    fn len(&self) -> PRes<u64>;
    fn set_len(&mut self, len: u64) -> PRes<()>;
}

impl SizeTool for File {
    fn len(&self) -> PRes<u64> {
        Ok(self.metadata()?.len())
    }
    fn set_len(&mut self, len: u64) -> PRes<()> {
        Ok(File::set_len(self, len)?)
    }
}

impl SizeTool for Cursor<Vec<u8>> {
    fn len(&self) -> PRes<u64> {
        Ok(self.get_ref().len() as u64)
    }
    fn set_len(&mut self, len: u64) -> PRes<()> {
        Ok(self.get_mut().resize(len as usize, 0))
    }
}

pub struct ReadPage {
    buff: Arc<Vec<u8>>,
    index: u64,
    exp: u8,
    pos: usize,
}
impl ReadPage {
    pub fn new(buff: Arc<Vec<u8>>, pos: usize, index: u64, exp: u8) -> ReadPage {
        ReadPage { buff, index, exp, pos }
    }
}

impl ReadPage {
    pub fn get_size_exp(&mut self) -> u8 {
        self.exp
    }

    pub fn slice(&self, len: usize) -> &[u8] {
        let final_len = self.pos + len;
        debug_assert!(final_len < self.buff.len());
        &self.buff[self.pos..final_len]
    }
}

impl Read for ReadPage {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let len = self.buff.len();
        let amt = cmp::min(self.pos, len);
        let read = Read::read(&mut &self.buff[(amt as usize)..len], buf)?;
        self.pos += read;
        Ok(read)
    }
}

impl PageOps for ReadPage {
    fn seek(&mut self, pos: u32) -> PRes<()> {
        self.pos = (pos + 2) as usize;
        Ok(())
    }
    fn get_index(&self) -> u64 {
        self.index
    }
    fn get_size_exp(&self) -> u8 {
        self.exp
    }
    fn clone_read(&self) -> ReadPage {
        ReadPage::new(self.buff.clone(), 2, self.index, self.exp)
    }
    fn clone_write(&self) -> Page {
        Page::new(self.buff.as_ref().clone(), 2, self.index, self.exp)
    }
    fn is_free(&self) -> PRes<bool> {
        Ok((self.buff[1] & 0b1000_0000) != 0)
    }
}

#[derive(Clone)]
pub struct Page {
    buff: Vec<u8>,
    index: u64,
    exp: u8,
    pos: usize,
}

pub trait PageOps {
    fn seek(&mut self, pos: u32) -> PRes<()>;
    fn get_index(&self) -> u64;
    fn get_size_exp(&self) -> u8;
    fn clone_read(&self) -> ReadPage;
    fn clone_write(&self) -> Page;
    fn is_free(&self) -> PRes<bool>;
}

fn free_flag_set(mut cur: u8, free: bool) -> u8 {
    if free {
        cur |= 0b1000_0000;
    } else {
        cur &= !0b1000_0000;
    }
    cur
}
fn is_free(cur: u8) -> bool {
    cur & 0b1000_0000 != 0
}
impl Page {
    pub fn new(buff: Vec<u8>, pos: usize, index: u64, exp: u8) -> Page {
        Page { buff, index, exp, pos }
    }
    pub fn clone_resetted(&self) -> Page {
        let mut page = self.clone();
        page.pos = 2;
        page
    }
}

impl Read for Page {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let len = self.buff.len();
        let amt = cmp::min(self.pos, len);
        let read = Read::read(&mut &self.buff[(amt as usize)..len], buf)?;
        self.pos += read;
        Ok(read)
    }
}

impl Write for Page {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let pre = self.pos;
        let len = self.buff.len();
        if pre + (*buf).len() > len {
            panic!(
                "Over page allowed content size:{}, data size: {}",
                len,
                pre + (*buf).len()
            );
        }
        let pos = cmp::min(self.pos, len);
        let amt = (&mut self.buff[(pos as usize)..len]).write(buf)?;
        self.pos += amt;
        Ok(amt)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.buff.flush()
    }
}

impl PageOps for Page {
    fn seek(&mut self, pos: u32) -> PRes<()> {
        self.pos = (pos + 2) as usize;
        Ok(())
    }
    fn get_index(&self) -> u64 {
        self.index
    }
    fn get_size_exp(&self) -> u8 {
        self.exp
    }
    fn clone_read(&self) -> ReadPage {
        ReadPage::new(Arc::new(self.buff.clone()), 2, self.index, self.exp)
    }
    fn clone_write(&self) -> Page {
        self.clone_resetted()
    }
    fn is_free(&self) -> PRes<bool> {
        Ok(is_free(self.buff[1]))
    }
}

impl Page {
    pub fn set_free(&mut self, free: bool) -> PRes<()> {
        self.buff[1] = free_flag_set(self.buff[1], free);
        Ok(())
    }

    pub fn set_next_free(&mut self, next: u64) -> PRes<()> {
        let pre = self.pos;
        self.pos = 2;
        self.write_u64::<BigEndian>(next)?;
        self.pos = pre;
        Ok(())
    }

    pub fn get_next_free(&mut self) -> PRes<u64> {
        let pre = self.pos;
        self.pos = 2;
        let val = self.read_u64::<BigEndian>()?;
        self.pos = pre;
        Ok(val)
    }

    pub fn reset(&mut self) -> PRes<()> {
        self.buff = vec![0; self.buff.len()];
        self.buff[0] = self.exp;
        Ok(())
    }

    pub fn make_read(self) -> ReadPage {
        ReadPage::new(Arc::new(self.buff), 2, self.index, self.exp)
    }
}

fn load_page<T: Read + Seek>(fl: &mut T, page: u64) -> PRes<ReadPage> {
    // add 2 to skip the metadata
    fl.seek(SeekFrom::Start(page))?;
    let exp = fl.read_u8()?;
    let size = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
    let mut ve = vec![0 as u8; size as usize];
    ve[0] = exp;
    fl.read_exact(&mut ve[1..size as usize])?;
    Ok(ReadPage::new(Arc::new(ve), 2, page, exp))
}

fn create_page_raw<T: SizeTool + Seek>(fl: &mut T, exp: u8) -> PRes<u64> {
    let offset = fl.seek(SeekFrom::End(0))?;
    let size: u64 = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
    fl.set_len(offset + size)?;
    Ok(offset)
}

fn load_page_raw<T: Read + Seek>(fl: &mut T, page: u64, size_exp: u8) -> PRes<Page> {
    let mut ve;
    let size;
    {
        fl.seek(SeekFrom::Start(page))?;
        size = (1 << size_exp) as u64; //EXP - (size_exp+size_mitigator);
        ve = vec![0 as u8; size as usize];
        fl.read_exact(&mut ve[0..size as usize])?;
    }
    Ok(Page::new(ve, 0, page, size_exp))
}

fn flush_page<T: Write + Seek>(fl: &mut T, page: &Page) -> PRes<()> {
    fl.seek(SeekFrom::Start(page.index))?;
    fl.write_all(&page.buff)?;
    Ok(())
}

fn create_page<T: Write + Seek>(fl: &mut T, exp: u8) -> PRes<Page> {
    let size: u64 = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
    let mut ve = vec![0 as u8; size as usize];
    ve[0] = exp;
    let offset = fl.seek(SeekFrom::End(0))?;
    fl.write_all(&ve)?; //exp
    Ok(Page::new(ve, 2, offset, exp))
}

fn mark_allocated<T: Write + Read + Seek>(fl: &mut T, page: u64) -> PRes<u64> {
    fl.seek(SeekFrom::Start(page + 2))?;
    // Free pages are a linked list reading the next page and return
    let next = fl.read_u64::<BigEndian>()?;
    fl.seek(SeekFrom::Start(page + 1))?;
    let mut moderator = fl.read_u8()?;
    moderator = free_flag_set(moderator, false);
    fl.seek(SeekFrom::Start(page + 1))?;
    fl.write_u8(moderator)?;
    Ok(next)
}

fn trim_or_free_page<T: Write + Read + Seek + SizeTool>(
    fl: &mut T,
    page: u64,
    update_list: &mut dyn UpdateList,
) -> PRes<()> {
    fl.seek(SeekFrom::Start(page))?;
    let exp = fl.read_u8()?;
    let size = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
    if page + size == fl.len()? {
        fl.set_len(page + 1)?;
    } else {
        let old = update_list.update(exp, page)?;
        fl.seek(SeekFrom::Start(page + 1))?;
        let mut mitigator = fl.read_u8()?;
        debug_assert!(!is_free(mitigator), "freeing: {} already freed ", page);
        mitigator = free_flag_set(mitigator, true);
        let mut data: [u8; 9] = [0; 9];
        data[0] = mitigator;
        BigEndian::write_u64(&mut data[1..9], old);
        fl.seek(SeekFrom::Start(page + 1))?;
        fl.write_all(&data)?;
        if exp >= 5 {
            fl.write_all(&[0u8; 16])?;
        }
    }
    Ok(())
}

#[cfg(unix)]
pub struct DiscRef {
    file: File,
    metadata_changed: AtomicBool,
}

#[cfg(unix)]
impl DiscRef {
    pub fn new(file: File) -> DiscRef {
        DiscRef {
            file,
            metadata_changed: AtomicBool::new(true),
        }
    }
}

#[cfg(unix)]
impl Device for DiscRef {
    fn load_page(&self, page: u64) -> PRes<ReadPage> {
        use std::os::unix::prelude::FileExt;
        let mut exp = [0u8];
        self.file.read_exact_at(&mut exp, page)?;
        let size = (1 << exp[0]) as u64; //EXP - (size_exp+size_mitigator);
        let mut ve = vec![0 as u8; size as usize];
        ve[0] = exp[0];
        self.file.read_exact_at(&mut ve[1..size as usize], page + 1)?;
        // add 2 to skip the metadata
        Ok(ReadPage::new(Arc::new(ve), 2, page, exp[0]))
    }

    fn load_page_raw(&self, page: u64, size_exp: u8) -> PRes<Page> {
        use std::os::unix::prelude::FileExt;
        let size = (1 << size_exp) as u64; //EXP - (size_exp+size_mitigator);
        let mut ve = vec![0 as u8; size as usize];
        self.file.read_exact_at(&mut ve, page)?;
        Ok(Page::new(ve, 0, page, size_exp))
    }

    fn flush_page(&self, page: &Page) -> PRes<()> {
        use std::os::unix::prelude::FileExt;
        self.file.write_all_at(&page.buff, page.get_index())?;
        Ok(())
    }

    fn create_page_raw(&self, exp: u8) -> PRes<u64> {
        let size: u64 = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
        let offset = self.file.len()?;
        self.file.set_len(offset + size)?;
        self.metadata_changed.store(true, Ordering::SeqCst);
        Ok(offset)
    }

    fn create_page(&self, exp: u8) -> PRes<Page> {
        use std::os::unix::prelude::FileExt;
        let size: u64 = (1 << exp) as u64; //EXP - (size_exp+size_mitigator);
        let mut ve = vec![0 as u8; size as usize];
        ve[0] = exp;
        let offset = self.file.metadata()?.len();
        self.file.write_all_at(&ve, offset)?; //exp
        self.metadata_changed.store(true, Ordering::SeqCst);
        Ok(Page::new(ve, 2, offset, exp))
    }

    fn mark_allocated(&self, page: u64) -> PRes<u64> {
        use std::os::unix::prelude::FileExt;
        let mut next_data = [0u8; 8];
        self.file.read_exact_at(&mut next_data, page + 2)?;
        // Free pages are a linked list reading the next page and return
        let mut mitigator = [0u8];
        self.file.read_exact_at(&mut mitigator, page + 1)?;
        mitigator[0] = free_flag_set(mitigator[0], false);
        self.file.write_all_at(&mitigator, page + 1)?;
        Ok(BigEndian::read_u64(&next_data))
    }

    fn sync(&self) -> PRes<()> {
        if self.metadata_changed.load(Ordering::SeqCst) {
            self.file.sync_all()?;
        } else {
            self.file.sync_data()?;
        }
        self.metadata_changed.store(false, Ordering::SeqCst);
        Ok(())
    }

    fn trim_or_free_page(&self, page: u64, update_list: &mut dyn UpdateList) -> PRes<()> {
        use std::os::unix::prelude::FileExt;
        let mut exp = [0u8];
        self.file.read_exact_at(&mut exp, page)?;
        let size = (1 << exp[0]) as u64; //EXP - (size_exp+size_mitigator);
        if page + size == self.file.len()? {
            self.file.set_len(page + 1)?;
            self.metadata_changed.store(true, Ordering::SeqCst);
        } else {
            let old = update_list.update(exp[0], page)?;
            let mut mitigator = [0u8];
            self.file.read_exact_at(&mut mitigator, page + 1)?;
            debug_assert!(!is_free(mitigator[0]), "freeing: {} already freed ", page);
            mitigator[0] = free_flag_set(mitigator[0], true);
            let mut data: [u8; 9] = [0; 9];
            data[0] = mitigator[0];
            BigEndian::write_u64(&mut data[1..9], old);
            self.file.write_all_at(&data, page + 1)?;
            if exp[0] >= 5 {
                self.file.write_all_at(&[0u8; 16], page + 10)?;
            }
        }
        Ok(())
    }
}

#[cfg(not(unix))]
pub struct FileHandler {
    file: File,
    metadata_changed: bool,
}

#[cfg(not(unix))]
pub struct DiscRef {
    file: Mutex<FileHandler>,
}

#[cfg(not(unix))]
impl DiscRef {
    pub fn new(file: File) -> DiscRef {
        DiscRef {
            file: Mutex::new(FileHandler {
                file,
                metadata_changed: false,
            }),
        }
    }
}

#[cfg(not(unix))]
impl Device for DiscRef {
    fn load_page(&self, page: u64) -> PRes<ReadPage> {
        load_page(&mut self.file.lock()?.file, page)
    }

    fn load_page_raw(&self, page: u64, size_exp: u8) -> PRes<Page> {
        load_page_raw(&mut self.file.lock()?.file, page, size_exp)
    }

    fn flush_page(&self, page: &Page) -> PRes<()> {
        flush_page(&mut self.file.lock()?.file, page)
    }

    fn create_page_raw(&self, exp: u8) -> PRes<u64> {
        let lock = &mut self.file.lock()?;
        lock.metadata_changed = true;
        create_page_raw(&mut lock.file, exp)
    }

    fn create_page(&self, exp: u8) -> PRes<Page> {
        let lock = &mut self.file.lock()?;
        lock.metadata_changed = true;
        create_page(&mut lock.file, exp)
    }

    fn mark_allocated(&self, page: u64) -> PRes<u64> {
        mark_allocated(&mut self.file.lock()?.file, page)
    }

    fn sync(&self) -> PRes<()> {
        let to_sync;
        let metadata_changed;
        {
            let mut lock = self.file.lock()?;
            to_sync = lock.file.try_clone()?;
            metadata_changed = lock.metadata_changed;
            lock.metadata_changed = false;
        }
        if metadata_changed {
            to_sync.sync_all()?;
        } else {
            to_sync.sync_data()?;
        }
        Ok(())
    }

    fn trim_or_free_page(&self, page: u64, update_list: &mut dyn UpdateList) -> PRes<()> {
        trim_or_free_page(&mut self.file.lock()?.file, page, update_list)
    }
}

pub struct MemRef {
    data: Mutex<Cursor<Vec<u8>>>,
}
impl MemRef {
    pub fn new() -> MemRef {
        MemRef {
            data: Mutex::new(Cursor::new(Vec::new())),
        }
    }
}

impl Device for MemRef {
    fn load_page(&self, page: u64) -> PRes<ReadPage> {
        load_page(&mut *self.data.lock()?, page)
    }

    fn load_page_raw(&self, page: u64, size_exp: u8) -> PRes<Page> {
        load_page_raw(&mut *self.data.lock()?, page, size_exp)
    }

    fn flush_page(&self, page: &Page) -> PRes<()> {
        flush_page(&mut *self.data.lock()?, page)
    }

    fn create_page_raw(&self, exp: u8) -> PRes<u64> {
        create_page_raw(&mut *self.data.lock()?, exp)
    }

    fn create_page(&self, exp: u8) -> PRes<Page> {
        create_page(&mut *self.data.lock()?, exp)
    }

    fn mark_allocated(&self, page: u64) -> PRes<u64> {
        mark_allocated(&mut *self.data.lock()?, page)
    }

    fn sync(&self) -> PRes<()> {
        Ok(())
    }

    fn trim_or_free_page(&self, page: u64, update_list: &mut dyn UpdateList) -> PRes<()> {
        trim_or_free_page(&mut *self.data.lock()?, page, update_list)
    }
}
#[cfg(test)]
mod tests {
    use super::{Device, DiscRef, MemRef, PageOps, UpdateList};
    use crate::error::PRes;
    use byteorder::{ReadBytesExt, WriteBytesExt};
    use tempfile::Builder;

    fn temp_disc_ref(name: &str) -> DiscRef {
        DiscRef::new(
            Builder::new()
                .prefix(name)
                .suffix(".persy")
                .tempfile()
                .unwrap()
                .reopen()
                .unwrap(),
        )
    }

    fn create_load_flush_page_device(device: &mut dyn Device) {
        let page = device.create_page(5).unwrap().get_index();
        let pg = device.load_page(page).unwrap();
        device.flush_page(&mut pg.clone_write()).unwrap();
    }

    fn set_get_next_free_device(device: &mut dyn Device) {
        let page = device.create_page(5).unwrap().get_index();
        let pg = &mut device.load_page(page).unwrap().clone_write();
        pg.set_next_free(30).unwrap();
        device.flush_page(pg).unwrap();
        let pg1 = &mut device.load_page(page).unwrap().clone_write();
        let val = pg1.get_next_free().unwrap();
        assert_eq!(val, 30);
    }

    fn get_size_page_device(device: &mut dyn Device) {
        let page = device.create_page(5).unwrap().get_index();
        let pg = &mut device.load_page(page).unwrap();
        let sz = pg.get_size_exp();
        assert_eq!(sz, 5);
    }

    fn write_read_page_device(device: &mut dyn Device) {
        let page = device.create_page(5).unwrap().get_index();
        {
            let pg = &mut device.load_page(page).unwrap().clone_write();
            pg.write_u8(10).unwrap();
            device.flush_page(pg).unwrap();
        }
        {
            let pg = &mut device.load_page(page).unwrap();
            let va = pg.read_u8().unwrap();
            assert_eq!(va, 10);
            let sz = pg.get_size_exp();
            assert_eq!(sz, 5);
        }
    }

    struct PanicCase {}
    impl UpdateList for PanicCase {
        fn update(&mut self, _: u8, _: u64) -> PRes<u64> {
            panic!("should not put the free in the free list")
        }
    }

    fn create_load_trim_device(device: &mut dyn Device) {
        let page = device.create_page(5).unwrap().get_index();
        let pg = &mut device.load_page(page).unwrap().clone_write();
        device.flush_page(pg).unwrap();
        device.trim_or_free_page(page, &mut PanicCase {}).unwrap();
        assert!(device.load_page(page).is_err());
    }

    struct IgnoreCase {}
    impl UpdateList for IgnoreCase {
        fn update(&mut self, _: u8, _: u64) -> PRes<u64> {
            Ok(0)
        }
    }

    fn create_not_trim_not_last_device(device: &mut dyn Device) {
        let page = device.create_page(5).unwrap().get_index();
        let _page_after = device.create_page(5).unwrap();
        let pg = &mut device.load_page(page).unwrap().clone_write();
        device.flush_page(pg).unwrap();
        device.trim_or_free_page(page, &mut IgnoreCase {}).unwrap();
        let load_page = device.load_page(page);
        assert!(load_page.is_ok());
        assert!(load_page.unwrap().is_free().unwrap());
    }

    #[test]
    fn create_load_flush_page_disc() {
        create_load_flush_page_device(&mut temp_disc_ref("disc_ref.raw"));
    }

    #[test]
    fn create_load_flush_page_memory() {
        create_load_flush_page_device(&mut MemRef::new());
    }

    #[test]
    fn set_get_next_free_disc() {
        set_get_next_free_device(&mut temp_disc_ref("set_free.raw"));
    }

    #[test]
    fn set_get_next_free_memory() {
        set_get_next_free_device(&mut MemRef::new());
    }

    #[test]
    fn get_size_page_disc() {
        get_size_page_device(&mut temp_disc_ref("get_size.raw"));
    }
    #[test]
    fn get_size_page_memory() {
        get_size_page_device(&mut MemRef::new());
    }

    #[test]
    fn write_read_page_disc() {
        write_read_page_device(&mut temp_disc_ref("write_read.raw"));
    }
    #[test]
    fn write_read_page_memory() {
        write_read_page_device(&mut MemRef::new());
    }

    #[test]
    fn create_load_trim_disc() {
        create_load_trim_device(&mut temp_disc_ref("disc_ref_trim.raw"));
    }
    #[test]
    fn create_load_trim_memory() {
        create_load_trim_device(&mut MemRef::new());
    }

    #[test]
    fn create_not_trim_not_last_disc() {
        create_not_trim_not_last_device(&mut temp_disc_ref("disc_ref_no_trim.raw"));
    }

    #[test]
    fn create_not_trim_not_last_memory() {
        create_not_trim_not_last_device(&mut MemRef::new());
    }
}
